package ru.t1.malyugin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.AbstractUserOwnedDTOModel;

import java.util.Collection;
import java.util.List;

public interface IUserOwnedDTORepository<M extends AbstractUserOwnedDTOModel> extends IDTORepository<M> {

    void add(@NotNull String userId, @NotNull M model);

    void addAll(@NotNull String userId, @NotNull Collection<M> modelList);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    long getSize(@NotNull String userId);

    void clear(@NotNull String userId);

    void update(@NotNull String userId, @NotNull M model);

    void remove(@NotNull String userId, @NotNull M model);

}