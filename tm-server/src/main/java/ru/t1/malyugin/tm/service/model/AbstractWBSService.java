package ru.t1.malyugin.tm.service.model;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.model.IWBSRepository;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.api.service.model.IWBSService;
import ru.t1.malyugin.tm.comparator.CreatedComparator;
import ru.t1.malyugin.tm.comparator.NameComparator;
import ru.t1.malyugin.tm.comparator.StatusComparator;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;
import ru.t1.malyugin.tm.model.AbstractWBSModel;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractWBSService<M extends AbstractWBSModel> extends AbstractUserOwnedService<M>
        implements IWBSService<M> {

    public AbstractWBSService(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    protected abstract IWBSRepository<M> getRepository(@NotNull EntityManager entityManager);

    @NotNull
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) {
        if (sort == null) return findAll(userId);
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IWBSRepository<M> repository = getRepository(entityManager);
            if (sort.getComparator() == CreatedComparator.INSTANCE) return repository.findAllOrderByCreated(userId);
            if (sort.getComparator() == NameComparator.INSTANCE) return repository.findAllOrderByName(userId);
            if (sort.getComparator() == StatusComparator.INSTANCE) return repository.findAllOrderByStatus(userId);
            return findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) {
        if (comparator == null) return findAll(userId);
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IWBSRepository<M> repository = getRepository(entityManager);
            if (comparator == CreatedComparator.INSTANCE) return repository.findAllOrderByCreated(userId);
            if (comparator == NameComparator.INSTANCE) return repository.findAllOrderByName(userId);
            if (comparator == StatusComparator.INSTANCE) return repository.findAllOrderByStatus(userId);
            return findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return;
        if (status != null) model.setStatus(status);
        update(userId, model);
    }

}