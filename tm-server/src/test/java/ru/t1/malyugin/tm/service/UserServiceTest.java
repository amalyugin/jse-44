package ru.t1.malyugin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.exception.field.EmailEmptyException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.LoginEmptyException;
import ru.t1.malyugin.tm.exception.field.PasswordEmptyException;
import ru.t1.malyugin.tm.exception.user.EmailExistException;
import ru.t1.malyugin.tm.exception.user.LoginExistException;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.util.HashUtil;

import static ru.t1.malyugin.tm.TestData.*;

@Category(UnitCategory.class)
public class UserServiceTest {

    @Before
    public void initTest() {
        for (int i = 1; i <= NUMBER_OF_USERS; i++) {
            @NotNull final User user = new User();
            user.setLogin("TST_" + i);
            user.setPasswordHash("TST");
            user.setEmail("tst@mail.ru" + i);
            USER_SERVICE.add(user);
            USER_LIST.add(user);

            @NotNull final Project project = new Project("NEW", "NEW");
            @NotNull final Task task = new Task("NEW", "NEW");
            PROJECT_SERVICE.add(user.getId(), project);
            TASK_SERVICE.add(user.getId(), task);
        }
    }

    @After
    public void clearData() {
        TASK_SERVICE.clear();
        PROJECT_SERVICE.clear();
        for (@Nullable final User user : USER_LIST) USER_SERVICE.remove(user);
        TASK_LIST.clear();
        PROJECT_LIST.clear();
        USER_LIST.clear();
    }

    @Test
    public void testCreateUser() {
        final long initialNumberOfUsers = USER_SERVICE.getSize();
        final long expectedNumberOfUsers = initialNumberOfUsers + 4;
        @NotNull final User user1 = USER_SERVICE.create("NEW1", "PASS1", "MAIL1", null);
        @NotNull final User user2 = USER_SERVICE.create("NEW2", "PASS2", "MAIL2", Role.ADMIN);
        @NotNull final User user3 = USER_SERVICE.create("NEW3", "PASS3", null, null);
        @NotNull final User user4 = USER_SERVICE.create("NEW4", "PASS4", null, null);
        USER_LIST.add(user1);
        USER_LIST.add(user2);
        USER_LIST.add(user3);
        USER_LIST.add(user4);

        Assert.assertEquals(expectedNumberOfUsers, USER_SERVICE.getSize());
        Assert.assertEquals("NEW1", user1.getLogin());
        Assert.assertEquals("MAIL1", user1.getEmail());
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, "PASS1"), user1.getPasswordHash());
        Assert.assertEquals(Role.USUAL, user1.getRole());
        Assert.assertEquals("NEW2", user2.getLogin());
        Assert.assertEquals("MAIL2", user2.getEmail());
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, "PASS2"), user2.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user2.getRole());
    }

    @Test
    public void testCreateUserNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(null, UNKNOWN_STRING, UNKNOWN_STRING, null));
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(null, UNKNOWN_STRING, UNKNOWN_STRING, Role.USUAL));

        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create(UNKNOWN_STRING, null, UNKNOWN_STRING, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create(UNKNOWN_STRING, null, UNKNOWN_STRING, Role.USUAL));

        @NotNull final User user = USER_LIST.get(0);
        Assert.assertThrows(LoginExistException.class, () -> USER_SERVICE.create(user.getLogin(), UNKNOWN_STRING, UNKNOWN_STRING, null));
        Assert.assertThrows(LoginExistException.class, () -> USER_SERVICE.create(user.getLogin(), UNKNOWN_STRING, UNKNOWN_STRING, Role.USUAL));

        Assert.assertThrows(EmailExistException.class, () -> USER_SERVICE.create(UNKNOWN_STRING, UNKNOWN_ID, user.getEmail(), null));
        Assert.assertThrows(EmailExistException.class, () -> USER_SERVICE.create(UNKNOWN_STRING, UNKNOWN_ID, user.getEmail(), Role.USUAL));
    }

    @Test
    public void testLockUserByLogin() {
        for (@NotNull final User user : USER_LIST) {
            USER_SERVICE.lockUser(user.getLogin());
            @Nullable final User actualUser = USER_SERVICE.findOneById(user.getId());
            Assert.assertNotNull(actualUser);
            Assert.assertEquals(true, actualUser.getLocked());
        }
    }

    @Test
    public void testLockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.lockUser(null));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.lockUser(UNKNOWN_STRING));
    }

    @Test
    public void testUnlockUserByLogin() {
        for (@NotNull final User user : USER_LIST) {
            USER_SERVICE.lockUser(user.getLogin());
            @Nullable User actualUser = USER_SERVICE.findOneById(user.getId());
            Assert.assertNotNull(actualUser);
            Assert.assertEquals(true, actualUser.getLocked());
            USER_SERVICE.unlockUser(user.getLogin());
            actualUser = USER_SERVICE.findOneById(user.getId());
            Assert.assertNotNull(actualUser);
            Assert.assertEquals(false, actualUser.getLocked());
        }
    }

    @Test
    public void testUnlockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.unlockUser(null));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.unlockUser(UNKNOWN_STRING));
    }

    @Test
    public void testRemove() {
        long expectedNumberOfUsers = USER_SERVICE.getSize();
        for (@NotNull final User user : USER_LIST) {
            expectedNumberOfUsers--;
            Assert.assertNotEquals(0, TASK_SERVICE.getSize(user.getId()));
            Assert.assertNotEquals(0, PROJECT_SERVICE.getSize(user.getId()));
            USER_SERVICE.removeById(user.getId());
            Assert.assertEquals(expectedNumberOfUsers, USER_SERVICE.getSize());
            Assert.assertEquals(0, TASK_SERVICE.getSize(user.getId()));
            Assert.assertEquals(0, PROJECT_SERVICE.getSize(user.getId()));
        }
    }

    @Test
    public void testRemoveByLogin() {
        long expectedNumberOfUsers = USER_SERVICE.getSize();
        for (@NotNull final User user : USER_LIST) {
            expectedNumberOfUsers--;
            Assert.assertNotEquals(0, TASK_SERVICE.getSize(user.getId()));
            Assert.assertNotEquals(0, PROJECT_SERVICE.getSize(user.getId()));
            USER_SERVICE.removeByLogin(user.getLogin());
            Assert.assertEquals(expectedNumberOfUsers, USER_SERVICE.getSize());
            Assert.assertEquals(0, TASK_SERVICE.getSize(user.getId()));
            Assert.assertEquals(0, PROJECT_SERVICE.getSize(user.getId()));
        }
    }

    @Test
    public void testRemoveByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.removeByLogin(null));
    }

    @Test
    public void testRemoveByEmail() {
        long expectedNumberOfUsers = USER_SERVICE.getSize();
        for (@NotNull final User user : USER_LIST) {
            expectedNumberOfUsers--;
            Assert.assertNotEquals(0, TASK_SERVICE.getSize(user.getId()));
            Assert.assertNotEquals(0, PROJECT_SERVICE.getSize(user.getId()));
            USER_SERVICE.removeByEmail(user.getEmail());
            Assert.assertEquals(expectedNumberOfUsers, USER_SERVICE.getSize());
            Assert.assertEquals(0, TASK_SERVICE.getSize(user.getId()));
            Assert.assertEquals(0, PROJECT_SERVICE.getSize(user.getId()));
        }
    }

    @Test
    public void testRemoveByEmailNegative() {
        Assert.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.removeByEmail(null));
    }

    @Test
    public void testSetPassword() {
        @NotNull final User user = USER_LIST.get(0);
        @NotNull final String pass = "NEWP";
        @Nullable final String passHash = HashUtil.salt(PROPERTY_SERVICE, pass);
        USER_SERVICE.setPassword(user.getId(), pass);
        @Nullable final User actualUser = USER_SERVICE.findOneById(user.getId());
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(passHash, actualUser.getPasswordHash());
    }

    @Test
    public void testSetPasswordNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.setPassword(null, UNKNOWN_STRING));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.setPassword(UNKNOWN_STRING, null));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.setPassword(UNKNOWN_STRING, UNKNOWN_STRING));
    }

    @Test
    public void testUpdateProfile() {
        for (@NotNull final User user : USER_LIST) {
            @NotNull final String test = "TEST";
            USER_SERVICE.update(user.getId(), null, null, test);
            USER_SERVICE.update(user.getId(), null, test, null);
            USER_SERVICE.update(user.getId(), test, null, null);
            @Nullable final User actualUser = USER_SERVICE.findOneById(user.getId());
            Assert.assertNotNull(actualUser);
            Assert.assertEquals(test, actualUser.getFirstName());
            Assert.assertEquals(test, actualUser.getMiddleName());
            Assert.assertEquals(test, actualUser.getLastName());
        }
    }

    @Test
    public void testUpdateProfileNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.update(null, UNKNOWN_STRING, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.update(UNKNOWN_STRING, UNKNOWN_STRING, UNKNOWN_STRING, UNKNOWN_STRING));
    }

    @Test
    public void testFindOneByLogin() {
        for (@NotNull final User user : USER_LIST) {
            Assert.assertEquals(user.getId(), USER_SERVICE.findOneByLogin(user.getLogin()).getId());
        }
    }

    @Test
    public void testFindOneByEmail() {
        for (@NotNull final User user : USER_LIST) {
            Assert.assertEquals(user.getId(), USER_SERVICE.findOneByEmail(user.getEmail()).getId());
        }
    }

    @Test
    public void testIsLoginExist() {
        for (@NotNull final User user : USER_LIST) {
            Assert.assertEquals(true, USER_SERVICE.isLoginExist(user.getLogin()));
        }
    }

    @Test
    public void testIsEmailExist() {
        for (@NotNull final User user : USER_LIST) {
            Assert.assertEquals(true, USER_SERVICE.isEmailExist(user.getEmail()));
        }
    }

}