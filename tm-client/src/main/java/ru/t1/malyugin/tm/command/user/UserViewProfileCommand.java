package ru.t1.malyugin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.UserDTO;
import ru.t1.malyugin.tm.dto.request.user.UserGetProfileRequest;
import ru.t1.malyugin.tm.enumerated.Role;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-show";

    @NotNull
    private static final String DESCRIPTION = "show current user profile";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER SHOW PROFILE]");

        @NotNull final UserGetProfileRequest request = new UserGetProfileRequest(getToken());
        @Nullable final UserDTO user = getUserEndpoint().getProfile(request).getUser();

        renderProfile(user);
    }

}