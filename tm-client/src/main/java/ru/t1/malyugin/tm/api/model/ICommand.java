package ru.t1.malyugin.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.enumerated.Role;

public interface ICommand {

    @Nullable
    String getArgument();

    @NotNull
    String getName();

    @NotNull
    String getDescription();

    void execute();

    @Nullable
    Role[] getRoles();

}